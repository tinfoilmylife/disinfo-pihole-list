A blocklist for fake news, heavily-politically biased news, some amlicious sites disguised as news, and generally trash websites that you might want to avoid. 
Some of these are quite popular sites, but they twist the facts to suit a narrative. Don't let your mind be poisoned! 
Personally, I don't want these sites to influence my thinking or get a single penny of ad-revenue from me. 
As far what news sources you should trust, well, you are on your own there as there aren't many that report straight facts. 

I tried to include sites that push nothing but social justice, fear mongering, far-right conspiracies, lizard people, aliens, etc. I left out sites like bloomberg because, while guilty of these things, there are actually some decent articles on there. 
The goal is to block sites that are primarily filled with narrative pushing and manipulation, not those that are occasionally guilty of it.

I'm actually taking the time to look at these sites (on a VPN in Iridium browser from a Linux box...aka full tinfoil hat mode).

I have also come to discover a handful of sites that are just flat out malicious, have immediate redirects, set off internet security alerts, etc and added them to the list. This list will be updated regularly for a good while.

Note that I am allowing politcally biased news sources because almost are are in some way, but am blocking the most extreme because they push propaganda and misinformation.

If you decide you want to block some of these sites, but you just can't (for whatever reason) live without access to the mainstream media sites, use the other list titled `disinfoList_without_MSM.txt` on step 4.

## Usage
1. Login into Pi-hole admin
2. Navigate to "Group Management"
3. Click on "Adlists"
4. Copy this URL: https://gitlab.com/tinfoilmylife/disinfo-pihole-list/-/raw/master/disinfoList.txt
  - OPTIONALLY, if you want mainstream sites like CNN, Salon, Vox, BuzzFeed, Infowars, etc., then use this URL instead: https://gitlab.com/tinfoilmylife/disinfo-pihole-list/-/raw/master/disinfoList_without_MSM.txt
5. Paste the URL in the "Address" box, optionally you can add a comment like "Tinfoilmylife.com" or something, and click on "Add"

## Changelogs
I wrote a script to add sites to the lists, make them all lower case, alphabetize them, and remove duplicates. I decided I could use the same script to generate changelogs for me.

If you are interested in seeing the changes made to the lists, you can view the changelog for each list here:
- for `disinfoList.txt` navigate to [https://gitlab.com/tinfoilmylife/disinfo-pihole-list/-/blob/master/changelog_disinfoList.md](https://gitlab.com/tinfoilmylife/disinfo-pihole-list/-/blob/master/changelog_disinfoList.md)
- for `disinfoList_without_MSM.txt` navigate to [https://gitlab.com/tinfoilmylife/disinfo-pihole-list/-/blob/master/changelog_without_MSM.md](https://gitlab.com/tinfoilmylife/disinfo-pihole-list/-/blob/master/changelog_without_MSM.md)