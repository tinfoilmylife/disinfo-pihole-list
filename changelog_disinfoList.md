## Added on 04/06/2021 06:30 am 
- fivethirtyeight.com

## Added on 03/28/2021 06:22 pm 
- usatoday.com
- reuters.com
- thefederalist.com
- theintercept.com

## Added on 03/28/2021 09:20 am 
- Urdoca.com
- USA-Television.com
- USADailyPost.us
- USADailyTime.com
- USADoseNews.com
- USAPolitics24hrs.com
- USAPoliticsToday.com
- USAPoliticsZone.com
- USHealthyAdvisor.com
- USInfoNews.com
- USANewsflash.com
- USPoliticsInfo.com
- USPOLN.com
- USASupreme.com
- ViralActions.com
- VoxTribune.com
- WestfieldPost.com
- WhyDontYouTryThis.com
- WorldPoliticsNow.com
- WRPM33.com

## Added on 03/27/2021 02:19 pm 
- ReadConservatives.news
- RedInfo.us
- ReflectionofMind.org
- Religionlo.com
- ReligionMind.com
- Revolutions2040.com
- SocialEverythings.com
- Spinzon.com
- States-TV.com
- Success-Street.com
- SupremePatriot.com
- TDTAlliance.com
- ThePremiumNews.com
- TheBigRiddle.com
- TheExaminer.site
- TheInternetPost.net
- TheMiamiGazette.com
- TheMoralOfTheStory.us
- TheNewYorkEvening.com
- TheRooster.com
- TheTrumpMedia.com
- TheUSA-News.com
- TheWashingtonPress.com
- TrueTrumpers.com

## Added on 03/27/2021 08:32 am 
- insider.com
- PatriotUSA.website
- Persecutes.com
- PoliticalMayhem.news
- PoliticsPaper.com
- PoliticsUSANews.com
- president45donaldtrump.com
- ProudLeader.com
- PuppetStringNews.com

## Added on 03/27/2021 07:48 am 
- LadyLibertysNews.com
- LearnProgress.org
- Local31News.com
- LondonWebNews.com
- MacedoniaOnline.eu
- MadWorldNews.com
- Mentor2day.com
- MetropolitanWorlds.com
- MIssissippiHerald.com
- NativeStuff.us
- NBC.com.co
- Nephef.com
- NewPoliticsToday.com
- News4KTLA.com
- NewsBreaksHere.com
- NewsBySquad.com
- newsbreakingspipe.com
- NewsDaily12.com
- NewsFeedHunter.com
- NewsLeak.co
- NewsJustForYou1.blogspot.com
- NewsOfTrump.com
- NewzMagazine.com
- NotAllowedTo.com
- Now8News.com
- OnePoliticalPlaza.com
- OurLandoftheFree.com

