const fs = require('fs');
const uniq = require('lodash/uniq');
const moment = require('moment');
const chalk = require('chalk');
const newAdditions = require('./newlyAdded').newAdditions;

const fileName = process.env.FILE;

/** Update list logic */
const fileData = fs.readFileSync(fileName, 'utf8').toString();
const listArray = fileData.split(/\r?\n/);
const withNewestAAdditions = [...listArray, ...newAdditions];
const convertedToLowerCase = withNewestAAdditions.map(item => item.toLowerCase()).sort();
const deduped = uniq(convertedToLowerCase);
const newList = deduped.join('\r\n');

try {
  fs.writeFileSync(fileName, newList);
  console.log(chalk.green(`${fileName} successfully written!`));
} catch (error) {
  console.error(chalk.red.bold(`THERE WAS AN ERROR WRITING ${fileName}!`, error));
}

/** Update changelog logic */
const dateTimeUSFormat = moment().format('MM/DD/YYYY hh:mm a');
const newAdditionsList = newAdditions.map(site => `- ${site}`);
const newAdditionsString = newAdditionsList.join('\r\n');

const changelogPath = fileName === 'disinfoList.txt' ? 'changelog_disinfoList.md' : 'changelog_without_MSM.md';
const changelog = fs.readFileSync(changelogPath, 'utf8').toString();
const newChangelogData = `## Added on ${dateTimeUSFormat} \r\n${newAdditionsString}\r\n\r\n${changelog}`;

try {
  fs.writeFileSync(changelogPath, newChangelogData);
  console.log(chalk.cyan(`${changelogPath} successfully updated!`));
} catch(error) {
  console.error(chalk.yellow.bold(`THERE WAS AN ERROR WRITING THE ${changelogPath}!`, error));
}

// only gonna increment semver for main list; otherwise would get duplicate instances of it being incremented
if (fileName === 'disinfoList.txt') {
  /** Increment semver logic in package.json */
  const packageJsonRaw = fs.readFileSync('package.json', 'utf8');
  const packageJson = JSON.parse(packageJsonRaw);
  const currentSemver = packageJson.version;
  // incrementing second number because there won't be breaking changes and nothing is really a hotfix
  // it is all just "feature adds" really
  const currentSemverArr = currentSemver.split('.');
  const newVersion = +currentSemverArr[1] + 1;
  currentSemverArr[1] = newVersion;
  const newSemverString = currentSemverArr.join('.');
  packageJson.version = newSemverString;
  
  try {
    fs.writeFileSync('package.json', JSON.stringify(packageJson, null, 2));
    console.log(chalk.blue('Successfully updated version in package.json!'));
  } catch(error) {
    console.error(chalk.magenta.bold('THERE WAS AN ERROR WRITING NEW VERSION TO PACKAGE.JSON!'));
  }
}